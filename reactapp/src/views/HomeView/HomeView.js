import React, { Component } from 'react'
import PropTypes  from 'prop-types'

import styles from './HomeView.scss'

class HomeView extends Component {
  static propTypes = {
    processing: PropTypes,
    election: PropTypes.arrayOf(PropTypes.object).isRequired
  }

  render () {
    const { processing } = this.props
    return (
      <div className={styles.home}>
        { processing ? <div className={styles.spinner}  /> : '' }
      </div>
    )
  }
}

const mapStateToProps = (store) => {
  let { elections, processing } = store.elections
  return {
    processing,
    elections,
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchElections,
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeView)

