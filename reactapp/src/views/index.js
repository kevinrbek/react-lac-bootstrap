// this file dynamically exports all of the common/components classes
const context = require.context('./', true, /\/.+\/index\.js$/)

const exportObj = {}
context.keys().forEach((key) => {
  const incl = context(key)
  Object.assign(exportObj, incl)
  if (incl.default) {
    const componentName = key.match(/\.\/(.+)\/index\.js/i)[1]
    exportObj[componentName] = incl.default
  }
})
module.exports = exportObj
