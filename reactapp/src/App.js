import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import { HomeView} from 'views'

import styles from './App.scss'

class App extends Component {
  render () {
    return (
      <div className={styles.app}>
        {/*Header Goes Here*/}
        <Switch>
          <Route exact path='/' component={HomeView} />
        </Switch>
      </div>
    )
  }
}

export default App
