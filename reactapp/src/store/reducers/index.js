import { combineReducers } from 'redux'
import _ from 'underscore'

const context = require.context('.', true, /^\.\/[^.]+(?!spec)\.js$/)
const reducers = {
  children: {},
  items: {
  },
}

context.keys().forEach((key) => {
  const incl = context(key)
  const pathParts = key.split('/').slice(1)

  let exportObj = reducers
  _.each(pathParts, (ch, i) => {
    if (pathParts.length - 1 === i) {
      return
    }
    let child = exportObj.children[ch]
    if (!child) {
      child = {
        name: ch,
        children: [],
        items: {},
      }
      exportObj.children[ch] = child
    }
    exportObj = child
  })

  if (incl.default) {
    const componentName = key.match(/([^/]+)\.js$/i)[1]
    exportObj.items[componentName] = incl.default
  }
})

const buildReducer = (obj) => {
  const childReducers = _.object(_.map(obj.children, (val, key) => [
    key,
    buildReducer(val),
  ]))
  const reducersToCombine = Object.assign({}, obj.items, childReducers)
  return combineReducers(reducersToCombine)
}
const rootReducer = buildReducer(reducers)
export default rootReducer
