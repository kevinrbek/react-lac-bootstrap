import api from '../api'
import * as ACTIONS from '../const'

export const fetchElections = () => {
  return (dispatch) => {
    dispatch({
      type: ACTIONS.ELECTIONS_FETCH,
    })
    return api('GET', '/api/elections').then(
      (elections) => {
        dispatch({
          type: ACTIONS.ELECTION_FETCH_COMPLETE,
          payload: { elections },
        })
      },
      (error) => {
        dispatch({
          type: ACTIONS.ELECTION_FETCH_COMPLETE,
          payload: { error },
        })
      }
    )
  }
}
