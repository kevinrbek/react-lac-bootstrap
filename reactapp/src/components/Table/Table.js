import React, { Component } from 'react'
import PropTypes from 'prop-types'
import _ from 'underscore'

import styles from './Table.scss'

class Table extends Component {
  static propTypes = {
    tableData: PropTypes.arrayOf(PropTypes.object).isRequired,
    tableHeaders: PropTypes.arrayOf(PropTypes.object).isRequired,
  };

  _renderTableHeader = (headerText, index) => {
    return (
      <td key={`${index}-header`}>{headerText}</td>
    )
  };

  _renderTableCell = (content, extraStyles, index) => {
    let styleClass = styles['table-cell']
    if (extraStyles) {
      styleClass = `${styleClass} ${extraStyles}`
    }
    return (
      <td key={`${index}-cell`} className={styleClass}>{content}</td>
    )
  };

  render () {
    let tableRows = []
    let tableHeaders = []

    _.each(this.props.tableHeaders, (header, i) => {
      tableHeaders.push(this._renderTableHeader(header.displayText, i))
    })

    _.each(this.props.tableData, (row, i) => {
      let rowCells = []
      _.each(this.props.tableHeaders, (header, j) => {
        const content = row[header.objectKey] || ''
        const extraStyles = header.extraStyles

        rowCells.push(this._renderTableCell(content, extraStyles, j))
      })

      tableRows.push(<tr key={i} className={styles['table-row']}>{rowCells}</tr>)
    })

    // TODO automate extra row addition
    for (let i = 0; i < 15; i++) {
      tableRows.push(<tr key={`${i}-row`} className={styles['table-row']}><td className={styles['empty-row']} /><td className={styles['empty-row']} /><td className={styles['empty-row']} /><td className={styles['empty-row']} /><td className={styles['empty-row']} /></tr>)
    }

    return (
      <table className={styles.table}>
        <thead className='table-header'>
          <tr className={styles['table-header-row']}>
            {tableHeaders}
          </tr>
        </thead>
        <tbody className='table-body'>
          {tableRows}
        </tbody>
      </table>
    )
  }
}

export default Table
