import 'whatwg-fetch'
import _ from 'underscore'

// this invokes an api call for a HTTP method to the endpoint using the params
const call = (method, endpoint, headers = {}, params = {}) => {
  const fetchObj = {
    method,
  }

  let modifiedEndpoint = endpoint

  if (['POST', 'PUT', 'PATCH'].includes(method)) {
    if (params instanceof window.FormData) {
      fetchObj.body = params
    } else {
      fetchObj.headers = {
        'Content-Type': 'application/json',
      }
      fetchObj.body = JSON.stringify(params)
    }
  } else {
    fetchObj.headers = headers

    if (_.keys(params).length > 0) {
      modifiedEndpoint +=
        `?${
          _.map(params, (val, key) => {
            if (val instanceof Array) {
              return _.map(val, v => `${encodeURIComponent(key)}=${encodeURIComponent(v)}`).join('&')
            }
            return `${encodeURIComponent(key)}=${encodeURIComponent(val)}`
          }).join('&')}`
    }
  }

  return new Promise((resolve, reject) => {
    fetch(`${modifiedEndpoint}`, fetchObj).then((response) => {
      if (response.status === 204) {
        return Promise.resolve()
      }
      return response.json().then(
        (data) => {
          if (response.status === 401 && data.code === 'SESSION_EXPIRED') {
            return Promise.reject(new Error('session expired'))
          } if (response.status >= 200 && response.status < 400) {
            return data
          }
          return Promise.reject(new Error(`Status: ${response.status} Code: ${data.code}`))
        },
        err => Promise.reject(err)
      )
    }).then(resolve, reject)
  })
}

export default call
