import { createReducer } from '../createReducer'
import { ELECTIONS_FETCH, ELECTION_FETCH_COMPLETE } from '../const'
export const initialState = {
  elections: [],
  processing: false,
  error: null,
}

export default createReducer(initialState, {
  [ELECTIONS_FETCH]: (state, payload) => {
    return Object.assign({}, state, {
      processing: true,
    })
  },
  [ELECTION_FETCH_COMPLETE]: (state, payload) => {
    let { elections, error } = payload

    if (error) {
      // handle error
      return Object.assign({}, state, {
        error,
        processing: false,
      })
    }

    return Object.assign({}, state, {
      elections,
      processing: false,
    })
  },
})
